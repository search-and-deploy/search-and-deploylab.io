---
title: "Intuitive coloring"
description: >
  Using the HSLa color model in CSS.
categories: posts
tags: [web, styling]
---

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## Color Model

HSLa is a color model supported by [common browsers](https://caniuse.com/#search=hsla){:rel="nofollow noopener noreferrer"}.
Its acronym stands for hue, saturation, lightness, and alpha. It's meant to be
intuitive, and edited by humans. Useful mostly when we are expected to match colors
rather than merely applying them. eg.

```css
background-color: hsla(210, 10%, 96%, 1);
```

From left to right, the params are:

- Hue (0 - 360). Position in the color wheel. More below.
- Saturation (0% - 100%). 0% grayscale. 100% full color.
- Lightness (0% - 100%). 0% for black. 100% for white. Start at 45%.
- Alpha (0 - 1). 0 is fully transparent, 0.5 see through, 1 opaque.

## Hue & The Color Wheel

![HSL color wheel flower shaped]({{ site.url }}/images/hsl-color-flower.svg)

Remembering the color wheel is relatively easy. Front, centered around 0&deg; (and 360&deg;),
covers reds. Right, centered around 120&deg;, includes greens. Left, centered
around 240&deg; has blues.

Taking the color wheel into account. Hue values go around it clockwise. Hence,
values around 0 & 360 covering reds.

## Tooling

- [HSLaExplorer](https://css-tricks.com/examples/HSLaExplorer/){:rel="nofollow noopener noreferrer"}
- [Contrast Ratio](https://leaverou.github.io/contrast-ratio/){:rel="nofollow noopener noreferrer"}.
- [W3C's Color Converter](https://www.w3schools.com/colors/colors_converter.asp){:rel="nofollow noopener noreferrer"}

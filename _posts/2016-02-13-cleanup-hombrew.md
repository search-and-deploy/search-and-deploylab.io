---
title: Clean up Homebrew
categories: bits
---
Ever wonder how much space are old homebrew bottles taking from your Mac?
Simply call `brew cleanup -n` from the terminal and you'll get the list
of bottles that would be removed as well as an approximation of how much
space would be free. If you actually want to remove them simply call `brew cleanup`.

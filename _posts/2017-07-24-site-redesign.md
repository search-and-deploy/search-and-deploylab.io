---
title: Search & Deploy's New Style
description: >
  An account of Search and Deploy's latest redesign process.
categories: posts
tags: [web, redesign]
---

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## Why

A few weeks back I noticed that a styling bug had leaked into the site and was
affecting all `code` elements. I was able to fixed the problem as soon as I
noticed it.

As I was getting re-acquainted with the site's code I realized how out of shape
it was. Following the code was more difficult than it needed to be. Time had
clearly come to clean Search & Deploy's code.

Through out this post I'll focus more on the process of redesigning S&D rather
that on the code. If you are curious about the code you are welcome to
check the [repo](https://notabug.org/rem/search-and-deploy) out.

## Dependencies

First, I upgraded Jekyll. I was expecting to run into a few problems since, at
the time I added collections to the site it was still an experimental feature.
Luckily, I had no trouble whatsoever.

## Collections

In the time I've been using Jekyll's collections my setup wasn't taking advantage
of them. Bits weren't included by default in the feed, and cheat sheets were
listed as posts, on top of having their own page. The new configuration fixes all
of that. I also added pagination to make exploring collections easier.

## Scaffolding

Even though the scaffolding improved I'm not entirely happy with it. Given Jekyll's
ways, I had to choose pretty urls over a nicer scaffolding; for now.

### Style Sheets

`css/main.css` holds configuration, and the sections import manifest. `_scss` has
all the section files in the main directory except for a few shared styles that I
decided to keep in `_base.scss` to avoid creating a `_partials` for such a small
amount of code. The `components` sub-directory includes the style sheets targeting,
mostly, individual html elements.

## Semantic HTML

One of the aspects that was in major need of maintenance was the HTML. Not only
it had plenty of `div`s, but it wasn't semantic nor accessible. While accessibility
compliance is far from the scope of this site I, at the very least, want to keep
all HTML semantic, so it's screen reader friendly.

## Nanogram

Style sheets also got a major upgrade. I dropped [Foundation](https://foundation.zurb.com/sites/download.html/){:rel="nofollow noopener noreferrer"}
as the styling framework. Long story short, I wanted to move S&D to a flexbox
model and neither Foundation, nor Bootstrap, were using it by default. Since S&D
was already using the bare minimum code from Foundation I decided to move to
something else.

Search & Deploy now uses Nanogram as its CSS nano framework. Nanogram is
[Milligram](https://milligram.io/){:rel="nofollow noopener noreferrer"}'s grid module, a modified version of
[Minireset.css](https://jgthms.com/minireset.css/){:rel="nofollow noopener noreferrer"}, as well as a custom version
of [Typebase](https://devinhunt.github.io/typebase.css/){:rel="nofollow noopener noreferrer"} for typography. It is
strongly influenced by [Gutenberg](https://matejlatin.github.io/Gutenberg/){:rel="nofollow noopener noreferrer"}, and
[CSS Purge](https://www.csspurge.com/){:rel="nofollow noopener noreferrer"}.

### The flexbox grid

Around the time I started fixing S&D I came across Milligram, a light weight CSS
framework based on the flexbox model. Since most new frameworks default to it,
and both major ones have announced using it by default for their next version, I
decided to move S&D to a flexbox model.

Originally, I intended to use milligram via their recommended CDN. Unfortunately,
the CDN wasn't keeping in line with browser settings, thus breaking the page's
style. Since Jekyll already has to pre-compile the rest of the style sheets, I
decided to include Milligram. Actually, in order to keep the compiled CSS file
small I only integrated some of its modules. This is a practice that even
Foundation encourages to follow with their own framework.

### Typography

In an effort to keep S&D's style minimalist rather than [brutalist](https://brutalistwebsites.com/){:rel="nofollow noopener noreferrer"},
I added Typebase to take care of typography. Since I wanted to stick to a
responsive design, I modified its sizing from pixels to `rem` and `em` for smoother
[responsive fonts](https://websemantics.uk/articles/responsive-fonts/demo/){:rel="nofollow noopener noreferrer"}.

### Code blocks

Jekyll handles code highlights through Rouge. While Rouge makes it easy to theme
color code blocks, finding suitable themes hasn't been easy in my experience.
Since this is one of the main areas that had been bothering for a while I got
[Tomorrow Night Blue](https://colorsublime.com/theme/Tomorrow_Night_Blue){:rel="nofollow noopener noreferrer"} working.

## Before and After

These are a few pictures showing Search & Deploy before and after being redesign.

![Cheat Sheets before shot]({{ site.url }}/images/redesign-july-2017/cheat-sheets-before-shot.png)
![Cheat Sheets after shot]({{ site.url }}/images/redesign-july-2017/cheat-sheets-after-shot.png)
![Close up cheat sheets before shot]({{ site.url }}/images/redesign-july-2017/close-up-cs-before.png)
![Close up cheat sheets after shot]({{ site.url }}/images/redesign-july-2017/close-up-cs-after.png)
![Close up site footer before shot]({{ site.url }}/images/redesign-july-2017/close-up-footer-before.png)
![Close up site footer after shot]({{ site.url }}/images/redesign-july-2017/close-up-footer-after.png)
![Bit with code block before shot]({{ site.url }}/images/redesign-july-2017/bit-code-block-before.png)
![Bit with code block after shot]({{ site.url }}/images/redesign-july-2017/bit-code-block-after.png)
![Posts page before shot]({{ site.url }}/images/redesign-july-2017/posts-page-before.png)
![Posts page after shot]({{ site.url }}/images/redesign-july-2017/posts-page-after.png)
![Close up code blocks before shot]({{ site.url }}/images/redesign-july-2017/close-up-code-block-before.png)
![Close up code blocks after shot]({{ site.url }}/images/redesign-july-2017/close-up-code-block-after.png)
![Close up cheat sheet table before shot]({{ site.url }}/images/redesign-july-2017/cheat-sheet-table-before.png)
![Close up cheat sheet table after shot]({{ site.url }}/images/redesign-july-2017/cheat-sheet-table-after.png)


## Resources

These are some of the resources that, over time, have shaped the practices, and
techniques used while redesigning Search & Deploy.

- [Keeping CSS specificity low](https://css-tricks.com/strategies-keeping-css-specificity-low/){:rel="nofollow noopener noreferrer"}
- [Code Guide by @mdo](https://codeguide.co/){:rel="nofollow noopener noreferrer"}
- [Object Oriented CSS](https://github.com/stubbornella/oocss/wiki/faq){:rel="nofollow noopener noreferrer"}
- [Principles of writing consistent, idiomatic CSS](https://github.com/necolas/idiomatic-css){:rel="nofollow noopener noreferrer"}
- [Gutenberg](https://matejlatin.github.io/Gutenberg/){:rel="nofollow noopener noreferrer"}
- [CSS purge](https://www.csspurge.com/){:rel="nofollow noopener noreferrer"}
- [Semantics and Accessibility](https://accessiblehtmlheadings.com/){:rel="nofollow noopener noreferrer"}
- [HTML5 Semantic Elements](https://www.w3schools.com/html/html5_semantic_elements.asp){:rel="nofollow noopener noreferrer"}
- [The Accessibility Cheatsheet](https://bitsofco.de/the-accessibility-cheatsheet/){:rel="nofollow noopener noreferrer"}
- [Flexbugs](https://github.com/philipwalton/flexbugs){:rel="nofollow noopener noreferrer"}
- [Using CSS Flexible Boxes](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Using_CSS_flexible_boxes){:rel="nofollow noopener noreferrer"}
- [Responsive Fonts](https://websemantics.uk/articles/responsive-fonts/demo/){:rel="nofollow noopener noreferrer"}

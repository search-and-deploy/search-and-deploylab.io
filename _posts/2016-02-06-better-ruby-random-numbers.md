---
title: Better Ruby random numbers
categories: bits
---
`rand` might be enough for generating arbitrary things. If you need to generate
a random number for something like a security token use the Secure Random library.
It's part of the Ruby standard library so there's no need for gems.

```ruby
require 'securerandom'

SecureRandom.hex  # => "3b5db98fa369b52545b626040403feb7"
```

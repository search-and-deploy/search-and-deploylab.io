---
title: Search Box
categories: bits
---

Search & Deploy now has a search box powered by [DuckDuckGo.](https://duckduckgo.com/){:rel="nofollow noreferrer noopener"}
Rather than embedding an `iframe` we've opted for adding an actual form.

```html
<form class="search-box" method="get" action="https://duckduckgo.com/">
  <input type="hidden" name="sites" value="search-and-deploy.gitlab.io">
  <input type="hidden" name="kh" value="1">
  <input type="hidden" name="kg" value="p">
  <input type="hidden" name="k1" value="-1">
  <input type="hidden" name="k7" value="#f4f5f6">
  <input type="hidden" name="k8" value="#373f51">
  <input type="hidden" name="k9" value="#006394">
  <input type="text" name="q" maxlength="255">
  <input type="submit" value="Search">
</form>
````

For privacy sake we've

- omit the partnership param (`t`).
- turn HTTPS on (`kh=1`)
- use the address bar as POST (`kg=p`)
- turned ads off (`k1=-1`)

Read more on the [hidden params](https://duckduckgo.com/params){:rel="nofollow noreferrer noopener"}

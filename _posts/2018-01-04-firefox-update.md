---
title: "Firefox Update"
categories: bits
---

_**Updated bit**_

We used to delete Activity Stream, Pocket, and Follow on Search extensions for
Firefox Developer Edition to actually disable them.

Since 63.07b none are longer available at their usual location. Instead, they are
now controlled via the `about:config` page.

Check out this repo for
[our opinionated Firefox settings](https://notabug.org/rem/.dots/src/master/firefox){:rel="nofollow noreferrer noopener"},
and sample [`user.js`](https://notabug.org/rem/.dots/src/master/firefox/user.js){:rel="nofollow noreferrer noopener"}
file, which disables the extensions mentioned above, among other things.

<br>

<details markdown="1" >

  <summary>
    Deprecated Bit
  </summary>

According to the [archilinux wiki](https://wiki.archlinux.org/index.php/Firefox/Privacy#Remove_system-wide_hidden_extensions){:rel="nofollow noreferrer noopener"}
we can safely remove a few hidden add-ons which firefox includes by default.
Beware, doing so will only remove these features until next update.

- activity stream. 'new tab' page replacement.
- pocket. proprietary freemium service add-on.
- follow on search. search telemetry.

on macOS:

```terminal
$ cd /Applications/FirefoxDeveloperEdition.app/Contents/Resources/browser/features
```

once there, remove the hidden add-ons simply by:

```terminal
$ rm activity-stream@mozilla.org.xpi firefox@getpocket.com.xpi followonsearch@mozilla.com.xpi
```

</details>

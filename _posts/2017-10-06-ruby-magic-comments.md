---
title: "Ruby Magic Comments"
description: >
  Ruby interpreter settings through comments.
categories: posts
tags: [ruby]
---

There are two special ways to setup Ruby settings at the very top of a file.

## Shebang

An interpreter directive mostly used on CLI programs, and scripts.

```ruby
#!/usr/bin/env ruby -E UTF-8:UTF-8 --enable-frozen-string-literal
```

## Option comments

These are expected at the very top of a file.

```ruby
# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
```

Find out more about ruby [encodings](https://ruby-doc.org/core-2.4.2/Encoding.html){:rel="nofollow noopener noreferrer"}.

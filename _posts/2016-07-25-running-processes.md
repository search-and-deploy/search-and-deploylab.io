---
title: Running processes
categories: bits
---

If we need to see which processes are currently running on
our mac box we can run from the terminal:

```shell
$ lsof -i -P
```

On linux:

```shell
$ sudo netstat -tunapl
```

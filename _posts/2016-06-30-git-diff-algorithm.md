---
title: Better Git diff algorithm
categories: bits
---

According to Git [documentation](https://git-scm.com/docs/diff-config){:rel="nofollow noopener noreferrer"}
the diff uses one of four algorithms.
According to this [SO explanation](https://stackoverflow.com/questions/32365271/whats-the-difference-between-git-diff-patience-and-git-diff-histogram){:rel="nofollow noopener noreferrer"}
the `histogram` algorithm has all of `patience`'s goodness, and it's slightly
faster than the default algorithm.

Add to your `~/.gitconfig`

```conf
[diff]
  algorithm = histogram
```

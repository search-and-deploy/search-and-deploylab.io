---
title: Array-argument & Splat Operator
categories: bits
---

If you use the splat operator to pass several parameters to a method, like so:

```ruby
def add(*numbers)
  numbers.inject(0) { |sum, number| sum + number }
end
```

Then, you can pass an Array prepended by the splat operator to the method and
it will work.

```ruby
add *[1,2,3,4] # => 10

some_numbers = [4, 6, 7, 8, 10]
add *some_numbers # => 35
```

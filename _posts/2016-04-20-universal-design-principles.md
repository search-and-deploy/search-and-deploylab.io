---
title: Universal Design Principles
description: >
  Universal Design principles for the web.
categories: posts
tags: [accessibility, web]
---

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## Intro

The guidelines for accessible content have four overarching principles. All
content should be perceivable, operable, understandable, and robust.
Most of these principles are easy to implement and won't have an impact on the
look and feel of a web site.

For more details check the W3C's [WCAG quick reference](https://www.w3.org/WAI/WCAG20/quickref/){:rel="nofollow noopener noreferrer"}.
That is, the Web Content Accessibility Guidelines.

## Perceivable

Information and user interface components must be presentable to users in ways
they can perceive.

- Create a simple and intuitive interface.
- Provide appropriate alternative text to non-text content.
- Caption and/or provide transcripts for media.
- Provide headers for data tables.

## Operable

User interface components and navigation must be operable.

- Ensure users can complete and submit all forms. Make all functionality
[keyboard accessible](https://webaim.org/techniques/keyboard/){:rel="nofollow noopener noreferrer"}.
- Give users enough time to read and use content.
- Help user navigate and find content.
- [hidden] Link to main content. Allow users to skip navigating elements.

## Understandable

Information and the operation of user interface must be understandable.

- Ensure links make sense out of context. Avoid 'click here', 'more', and the like.
- Do not rely on color alone to convey meaning.
- Help users avoid and correct mistakes. Such as, form validation.

## Robust

Content must be robust enough that it can be interpreted reliably by a wide
variety of user agents, including assistive technologies.

- Provide appropriate document structure. Outline (chrome extension).
- Ensure accessibility for non-HTML content. [PDF](https://webaim.org/techniques/acrobat/){:rel="nofollow noopener noreferrer"},
[Word](https://webaim.org/techniques/word/){:rel="nofollow noopener noreferrer"}, [Powerpoint](https://webaim.org/techniques/powerpoint/){:rel="nofollow noopener noreferrer"},
and so on.
- Make ECMAScript (JS) accessible.
    * Ensure accessibility through keyboard or assistive technologies.
    * Make sure no relevant content is hidden from assistive technologies.
    * Don't compromise user control over automated content changes.
    * Don't disable or mess with normal browser functionality.

---
title: "5 things to check with your tests"
categories: bits
---

Unit tests should always check for these five scenarios:

1. When there is 0 (zero).
2. When there is 1.
3. When there are a few.
4. When there are many.
5. Oops (There's an exception).

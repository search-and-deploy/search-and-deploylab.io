---
title: "Navigation Updates"
description: >
  Brief note on Search & Deploy most recent updates focused on navigation.
categories: posts
tags: [web, redesign]
---

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## Why

Search & Deploy has been tweaked over the course of the year. Most adjustments
have been on accessibility, and security, hence may have gone unnoticed.
Yet, we've kept a list of things that could use a hand but may require more time.
Although there's plenty to be fixed still, these are a few of the latest changes.

## General

We updated the layout for listings such as cheat sheets, posts, and playlists,
for one with better whitespace distribution. Thus, slimming the looks of the
affected pages. We also improved how links are display when focused.

## TOCs

It has caught our attention that scrolling so much, like when accessing the
bottom part of a cheat sheets, can make some users dizzy. To help reduce scrolling
we added table of contents to all the sections/items that may benefit from one.

## Bits

The updated Bits section discourages bookmarking individual bits. As mention
elsewhere, most bits are items that are still finding a more permanent home within
Search & Deploy. To help navigating them, though, we added both TOC, as well as a
top pagination panel to avoid scrolling as much as possible.

## Playlists

Whilst we kept the Playlists page, each playlist is now an index on itself. This
was done to avoid loading all videos at once. Thus improving loading time for
each playlist. Yet, to help keep data usage low we now embed each video on its
own page rather than just redirecting to a visually busy site.

## Privacy

Not all updates have being at the navigation/front level. Here are some of the
privacy-center changes that S&D has seen lately.

For a while now, S&D has had its own minimalist custom style. Yet, it used to
depend on 'live' Google fonts for its typeface. Since they can't be control via
Content Security Policy we've opted for using system fonts by default.

According to [Qualys SSL Report](https://www.ssllabs.com/ssltest/analyze.html?d=search-and-deploy.gitlab.io){:rel="nofollow noreferrer noopener"},
as well as the [Web Privacy Check by Dataskydd.net](https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fsearch-and-deploy.gitlab.io%2F){:rel="nofollow noreferrer noopener"}
S&D respects visitors privacy. Yet, when the [headers are strictly checked](https://securityheaders.com/?q=https%3A%2F%2Fsearch-and-deploy.gitlab.io%2F&hide=on&followRedirects=on){:rel="nofollow noreferrer noopener"}
it fails for not redirecting to `HTTPS`. It also fails due to missing basic `HTTP`
security headers such as `Strict-Transport-Security`, and `X-Content-Type-Option`.
Unfortunately, these won't really get fixed until S&D gets a more permanent home
away from free hosting.

To further improve visitors privacy, we've adopted the `nofollow`, and `noreferrer`
attributes to most, if not all external links. Also, we embed external media
using their no-cookie option when available, like with YouTube videos.

## Closing

Finding a simple way to create pages from datafiles proofed harder than expected.
Luckily, after searching the web for a while, we came across a ready-to-use
solution. Whilst this also proofed that Jekyll's popularity makes it relatively
easy to find plugins, it also adds to our list of reasons to switch to a more
flexible system.

We've already been exploring other static pages generators. Yet the fact remains
that since most libraries parse markdown differently changing generators might
require more effort than what markdown deserves. Chances are that until then
a temporary solution supporting a more robust markup language may be better.
<small>*\*cof\** org-mode *\*cof\**</small>

---
title: "Brew Reinstall PostgreSQL"
categories: bits
---

Unfortunately, in order to reinstall a local macOS instance of PostgreSQL from
scratch `brew uninstall` by itself won't be enough. Instead,

check if there's an instance:

```terminal
$ launchctl list | grep -i sql
```

Stop it with:

```terminal
$ brew services stop postgres
```

Remove PostgreSQL, and all related files:

```terminal
$ brew uninstall --force postgres
$ rm -rf /usr/local/var/postgres
$ rm -rf .psql_history .psqlrc .psql.local .pgpass .psqlrc.local
$ brew cleanup
```

Confirm through:

```terminal
$ brew list | grep sql
```

Re-install PostgreSQL

```terminal
$ brew install postgresql
```

This won't restore the `postgres` superuser if it's been dropped before.

---
title: PG::ConnectionBad
categories: bits
---

On occasion the local postgres server doesn't get turn off properly when
restarting the computer after updating software. Usually when this happens we get
a `PG::ConnectionBad` exception. If on a Mac do:

```shell
# On the command line
$ cd /usr/local/var/postgres/

# Look for FATAL: lock file "postmaster.pid" already exists
# HINT:  Is another postmaster running in data directory "/usr/local/var/postgres"?
$ less server.log

# Remove offender file
$ rm postmaster.pid

# Restart local postgres server (if necessary)
$ postgres -D /usr/local/var/postgres
```

On other systems such as GNU/Linux, most likely will find the offender file on
`/usr/var/postgres/`.

---
title: macOS IPv6 off
categories: bits
---

On macOS we can turn Wi-Fi's IPv6 off from the terminal:

```terminal
$ networksetup -setv6off Wi-Fi
```

If prompted, we need to authorize with our password.

---
title: Test Modules in Minitest
description: >
  How to test modules in Ruby, and Minitest
categories: posts
tags: [ruby, testing]
---

In order to test an uncoupled module you should extend the singleton class of
an `Object`.

```ruby
class MyClassTest < Minitest::Test
  def setup
    @test_object = Object.new
    @test_object.extend(Mixable) # has a 'mix?' method
  end

  def test_mixes_correctly
    assert @test_object.mix?
  end
end
```

When it comes to coupled modules --those that depend on the
implementation of a particular method-- the best thing to do in order to
show how it could be implemented is to use a dummy object.

```ruby
class DummyObject
  include OtherMixable # its 'mix' method depends on 'necessary_method'

  def necessary_method
    # your sample implementation
  end
end

class MyClassTest < Minitest::Test
  def test_my_object_mixes
    my_object = DummyObject.new
    assert my_object.mix
  end
end
```

Whenever you test a class that includes or extends a module you only need to
test the implementation of the method it depends on.
Other unit tests, as well as integration tests, should warn you if a module
hasn't been called correctly.

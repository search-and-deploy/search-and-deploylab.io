---
title: Autotest only recognizes .rb files
categories: bits
---
Minitest-autotest throws a `LoadError` exception if the file containing the functions
to be tested doesn't have the `.rb` extension.

**Reference**: Minitest-autotest [source code](https://github.com/seattlerb/minitest-autotest/blob/5216da46ff3f1ae037fa58568fa6be3de0368f4b/lib/autotest.rb#L186-L195){:rel="nofollow noopener noreferrer"}.

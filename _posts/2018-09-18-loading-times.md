---
title: "Loading Times"
categories: bits
---

_End-user machine times, as opposed to developer machine times_

A typical Ruby MVC app takes

| Server response time | Status |
|----------------------|--------|
| Avg < 50 ms          | Fast   |
| 50 ms > Avg < 300 ms | Avg    |
| Avg >= 300 ms        | Slow   |

For the front,

| Browser load time   | Status |
|---------------------|--------|
| Avg < 500 ms        | Fast   |
| 500 ms >= Avg < 2 s | Avg    |
| Avg >= 2 s          | Slow   |

Simple JSON API servers take

| Server response time | Status |
|----------------------|--------|
| Avg < 25 ms          | Fast   |
| 25 ms > Avg < 150 ms | Avg    |
| Avg >= 150 ms        | Slow   |

---
title: "Local Ruby Server"
categories: bits
---

Browsers avoid executing anything originating directly from the file system.
Rather than gem installing anything to serve a few files locally spin a WEBrick
instance from the terminal using Ruby's [Un](https://github.com/ruby/ruby/blob/trunk/lib/un.rb){:rel="nofollow noopener noreferrer"}
library.

```shell
$ cd dir/with/files
$ ruby -run -e httpd . -p 8080
```

Visit `localhost:8080` to play with the served files.

Learn more about `Un`, reading it's, pretty much, [hidden documentation](https://ruby-doc.org/stdlib-1.9.3/libdoc/un/rdoc/un_rb.html){:rel="nofollow noopener noreferrer"}.

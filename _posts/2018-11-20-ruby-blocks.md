---
title: Ruby blocks
categories: bits
---

In Ruby we can write blocks either pairing `do`... `end` or `{}`.
Although, at times, is necessary to switch one for the other. We may
consider them syntactically equivalent, and restrict their use to show
their intent.

We can show a block's functional intent by using `{}`. That is, when we
care about the block's returned value, but also that it doesn't change
the system's state.

When the block mutates the system's state, and we don't care about
the returned value, we'd use `do`...`end`.

If a block is doing both, returning a significant value and changing the
state of the system, consider separating any commands from queries being
made.

---
title: "ri"
description: >
  Ruby documentation through the ri CLI, and console.
categories: posts
tags: [ruby, terminal]
---

**TL;DR**

Ruby comes with a built-in namespace index which can be access through the `ri`
CLI, and console.

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## List

To list all namespaces `ri` knows about simply

```terminal
$ ri -l
```

We can also list all classes, and modules within a given namespace.

```terminal
$ ri Psych -l
```

## Read

```terminal
$ ri Hash # for full namespace
$ ri 'String::new' # for class methods
$ ri 'Array#<<' # for instance methods
$ ri 'Array.[]' # for class and instance methods
```

_Note the use of quotes when querying for method documentation._

## Add

Expand the `ri` documentation library with

```terminal
$ gem install minitest --document ri
$ gem update minitest --document ri
```

Alternatively, add to the local development `.gemrc` file

```config
install: --document ri
update: --document ri
```

Unused documentation gets removed automatically with:

```terminal
$ gem cleanup
```

## Include

By default, `ri` includes Ruby core and standard libraries, as well as `site_lib`,
RubyGems documentation, and `~/.rdoc`. Other directories may be added via:

```terminal
$ ri SomeNamespace -d=path/to/local/doc/dir
```

## Rebuild

We can rebuild the index for any gem with

```terminal
$ gem rdoc <gem_name> --ri --no-rdoc --overwrite
```

or for all at once with

```terminal
$ gem rdoc --all --ri --no-rdoc --overwrite
```

## Console

To start a console session simply run `ri` at the terminal without any arguments.
To end the `ri` session simply hit `ENTER` on an empty console prompt.

## More

```terminal
$ ri -h
$ man ri
```

---
title: "Debug ZSH Config"
categories: bits
---

List ZSH config files for debugging purposes:

```shell
zsh -o SOURCE_TRACE
```

**Source**:

- [ZSH Scripts & Functions](https://zsh.sourceforge.net/Doc/Release/Options.html#Scripts-and-Functions){:rel="nofollow noreferrer noopener"}

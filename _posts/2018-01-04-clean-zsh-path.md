---
title: "Clean ZSH PATH"
categories: bits
---

Remove `PATH` duplicate entries. Add to `.zshrc`:

```config
typeset -U PATH path
```

**Source**:

- [ZSH Shell Built-in Commands](https://zsh.sourceforge.net/Doc/Release/Shell-Builtin-Commands.html#Shell-Builtin-Commands){:rel="nofollow noreferrer noopener"}

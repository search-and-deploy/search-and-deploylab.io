---
title: "Minitest's refute"
categories: bits
---

Minitest's `refute` checks for 'not true' (falsy) rather than for `false`. If we
want our tests to fail when `nil` comes around perhaps, rather
than testing for: `refute boolean?` tests should check for
both: `refute boolean?` and `refute_nil boolean?`

Both refutations could be replaced with `assert_equal false, boolean?`.
In which case it would be better to use
`assert_equal true, boolean?` rather than `assert boolean?`
for consistency.

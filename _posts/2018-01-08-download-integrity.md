---
title: "Download Integrity"
categories: bits
---

Some FLOSS provide md5 hashes to verify the integrity of the what we are about
to download (installers, source code, etc.). On macOS, using ZSH, we can validate
the integrity of our file with:

```terminal
$ [ "$(md5 -q /path/to/file)" = "c7537c3cf1d5d2fc2b744af8025c0cf3" ] && (echo "valid")
```

_Where `c7537c3cf1d5d2fc2b744af8025c0cf3` would be the md5 hash provided by the
developers._

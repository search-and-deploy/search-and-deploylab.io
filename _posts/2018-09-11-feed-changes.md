---
title: "Feed Changes"
categories: bits
---

*A brief notice for receiving this via RSS*.

With all the changes this little site has been through lately, we forgot to
mention that we have effectively moved our feed from a RSS to Atom.

If you subscribed to S&D's Feed before July 22nd you'll stop receiving new posts,
and bits on October 1st.

You can already subscribe to our new Atom feed. Simply add
https://search-and-deploy.gitlab.io/atom.xml to your reader.

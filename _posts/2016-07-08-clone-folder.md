---
title: Clone folder
categories: bits
---

We can clone a folder with all it's contents, including hidden files,
as well as preserving all file attributes with:

```shell
$ cp -a /path/original/folder/. /cloned/folder/
```

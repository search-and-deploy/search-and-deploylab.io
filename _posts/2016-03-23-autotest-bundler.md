---
title: Let Bundler Handle Autotest
categories: bits
---
To let [Bundler](https://bundler.io/){:rel="nofollow noopener noreferrer"} handle
[Minitest-autotest](https://github.com/seattlerb/minitest-autotest){:rel="nofollow noopener noreferrer"} just make
sure to include `gem minitest-autotest` in the Gemfile. This will put the gem
and its dependencies on the load path. That gets rid of the
`invalid option: --server` exception you see otherwise.

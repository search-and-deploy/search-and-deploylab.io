# Search and Deploy
Repo for Search and Deploy blog.

## How is made?
I use Jekyll as the static site generator. Liquid as the markup language
for `layouts` and `includes`. Sass for styling, and Markdown for writing most entries.

## Why?
Just to have a repository of my notes on web security, privacy, and accessibility
from a developer perspective.

## Licenses

Except where noted:

- Code licensed ISC.
- Other content licensed CC-By-SA.

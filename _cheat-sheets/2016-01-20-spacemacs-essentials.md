---
description: >
  Spacemacs key bindings mostly for working with code.
  Navigate text. Manipulate text. Substitutions. Selection. iEdit mode. Comments.
  Searching. Find and replace. Execute external commands. Keyboard macros.
title: "Spacemacs Essentials"
---

*Version*: 0.200.XX

<details markdown="1" id="table-of-contents">
  <summary>
    Table of Contents
  </summary>

  * TOC
  {:toc}
</details>

## Navigate

<div markdown="1" class="table">

| Description             | Key Binding |
|-------------------------|-------------|
| Up one paragraph        | `{`         |
| Down one paragraph      | `}`         |
| Previous function       | `[[`        |
| Next function           | `]]`        |
| Outer brace up          | `[{`        |
| Outer brace down        | `]}`        |
| Back to last edition    | `g ;`       |
| Go to file under cursor | `g f`       |

</div>


## Manipulate

<div markdown="1" class="table">

| Description                                 | Key Binding        |
|---------------------------------------------|--------------------|
| Tabs to Spaces                              | `SPC SPC untabify` |
| Show whitespace                             | `SPC t w`          |
| Delete trailing whitespace                  | `SPC x d w`        |
| Split at cursor; new line                   | `SPC j n`          |
| Replace character under cursor with \<char> | `r <char>`         |
| Move line downwards                         | `SPC x J`          |
| Move line upwards                           | `SPC x K`          |
| Transpose current with previous char        | `SPC x t c`        |
| Transpose current with previous word        | `SPC x t w`        |
| Transpose current with previous line        | `SPC x t l`        |
| New indented line below                     | `SPC i j`          |
| New indented line above                     | `SPC i k`          |
| New line below                              | `SPC i J`          |
| New line above                              | `SPC i K`          |

</div>

## Substitute

Each of these commands should end with Return (`RET`) to be executed.

<div markdown="1" class="table">

| Description                                   | Key Binding                   |
|-----------------------------------------------|-------------------------------|
| \<old> word for \<new>                        | `:s/<old>/<new>`              |
| Every \<old> word for \<new> in a line        | `:s/<old>/<new>/g`            |
| Every \<old> for \<new> from \<ln1> to \<ln2> | `:<ln1>,<ln2>s/<old>/<new>/g` |
| Every \<old> for \<new> in whole buffer       | `:%s/<old>/<new>/g`           |
| (As above, confirm each time)                 | `:%s/<old>/<new>/gc`          |
| \<find> and \<replace> text from current line | `:.,$s/<find>/<replace>/gc`   |

</div>

## Select

_AKA Visual Mode_

<small markdown="1">
  *Note:* _`<matched>` may be any char of the pairs `""`, `''`, `()`, `{}`, `[]`_
</small>

First, start visual mode with `v`. Then, use any of

<div markdown="1" class="table">

| Description                         | Key Binding     |
|-------------------------------------|-----------------|
| Select \<matched> contents          | `v i <matched>` |
| Select matching parentheses         | `v a b`         |
| Select paragraph                    | `v i p`         |
| Enter select line mode              | `V`             |
| Enter char column(s) selection mode | `C-v`           |
| Enter expand region mode            | `SPC v`         |
| Wrap region in \<char>              | `s <char>`      |
| Indent region                       | `SPC j =`       |
| Select up to excluded \<char>       | `t <char>`      |
| Select up to included \<char>       | `f <char>`      |

</div>

## iEdit Mode

<div markdown="1" class="table">

| Description                                   | Key Binding            |
|-----------------------------------------------|------------------------|
| Init iEdit Mode*                              | `SPC s e`              |
| Go back to Command mode                       | `ESC` or `C-g`         |
| Limit scope to current function               | `F`                    |
| Limit scope to current line                   | `L`                    |
| Increase scope to line below                  | `J`                    |
| Increase scope to line above                  | `K`                    |
| Toggle visibility of lines w/o occurrences    | `V`                    |
| Navigate between occurrences                  | `n` or `N`             |
| Toggle occurrence selection                   | `M-;`                  |
| Prefix index to occurrences                   | `#` (`SPC u` start at) |
| Substitute occurrences (delete switch insert) | `S`                    |
| Replace occurrences with last yanked text     | `p`                    |
| Delete all occurrences                        | `D`                    |

</div>

<small>
\* We can also select the region we want to edit before starting this mode.
</small>


## Undo tree

<div markdown="1" class="table">

| Description                    | Key Binding |
|--------------------------------|-------------|
| Open undo tree                 | `SPC a u`   |
| Navigate tree up/down          | `j` / `k`   |
| Switch branches                | `h` / `l`   |
| Quit in current state          | `q`         |
| Abort changes                  | `C-q`       |
| Toggle diff                    | `d`         |
| Toggle timestamps              | `t`         |
| Toggle keyboard selection mode | `s`         |
| Scroll left                    | `,` and `<` |
| Scroll right                   | `.` and `>` |

</div>

To read the full documentation first do `SPC SPC finder-comentary`.
Then `undo-tree RET`.


## Comments

<div markdown="1" class="table">

| Description              | Key Binding |
|--------------------------|-------------|
| Comment selected line(s) | `SPC c l`   |
| Comment paragraph        | `SPC c p`   |
| Comment blocks           | `SPC ; RET` |

</div>

## Searching

<div markdown="1" class="table">

| Description                              | Key Binding       |
|------------------------------------------|-------------------|
| Forward search \<pattern>                | `/<pattern> RET`  |
| Backward search \<pattern>               | `?<word> RET`     |
| Next occurrence of word under cursor     | `*`               |
| Previous occurrence of word under cursor | `#`               |
| Turn off search highlight                | `:nohl RET`       |
| Next occurrence of search/token          | `n`               |
| Previous occurrence of search/token      | `N`               |
| Go to original token                     | `' '`             |
| Find matching parentheses (cursor on it) | `%`               |
| Find file in project                     | `SPC p f`         |
| Find <text> in project                   | `SPC /<text> RET` |
| Find text under cursor in project        | `SPC *`           |
| Resume last search                       | `SPC s l`         |

</div>

## Find & replace

### On a file

* Enter find & replace mode `M-%`
* Enter query
* Enter replacement
* Accept each replacement `y`
* Accept all replacements `!`
* Quit mode `q`


### On several files

**Note:** Make sure to have any of `ag`, `pt` or `ack` installed.

* Search `SPC /`
* Enter edit mode `C-c C-e`
* Go to occurrence, enter iedit `SPC s e`
* Edit occurrences, leave iedit
* Finalize `C-c C-c`

## Commands

There are three main ways to run commands (\<cmd>) in Spacemacs.

- `SPC SPC <cmd>`
- `M-x <cmd>`
- `:<cmd>`

The first two, invoke the helm mini-buffer, which can be used to search for the
right command. The last one, known as 'ex command', expects the command and its
arguments.

### Ex commands

All ex commands must begin with `:`, and end with `RET`.

#### Files

<div markdown="1" class="table">

| Description                     | Key Binding |
|---------------------------------|-------------|
| show file info                  | `:f`        |
| show line number                | `:=`        |
| save current file               | `:w`        |
| save also as \<file> (same dir) | `:w <file>` |
| save and quit                   | `:xa`       |
| save modified and close         | `:x`        |
| save and close                  | `:wq`       |
| quit                            | `:q`        |
| quit all                        | `:qa`       |

</div>


#### GUI

<div markdown="1" class="table">

| Description             | Key Binding |
|-------------------------|-------------|
| search highlight off    | `:noh`      |
| split window horizontal | `:sp`       |
| split window vertical   | `:vs`       |
| delete window           | `:clo`      |
| delete other windows    | `:on`       |

</div>

#### Special usages

<div markdown="1" class="table">

| Description                           | Key Binding                          |
|---------------------------------------|--------------------------------------|
| execute shell \<command>              | `:! <command>`                       |
| \<new_file> from \<line1> to \<line2> | `:<line1>,<line2> w <new_file>`      |
| insert \<file> at cursor point        | `:r <file>`                          |
| embed resource at cursor point        | `:r !curl --silent https://some.url` |
| list buffer registers                 | `:registers`                         |
| list buffer jumps                     | `:ju`                                |

</div>


For more commands check out Ag Ibragimov's
[gist with all ex commands](https://gist.github.com/agzam/acd3b5e311f8f7e87e21b550856f65e1){:rel="nofollow noreferrer noopener"}


## Keyboard Macros

A macro is a recording of a sequence of keyboard keystrokes, mouse actions, or
menu actions, that is bound to a command.

<div markdown="1" class="table">

| Description             | Keybinding     |
|-------------------------|----------------|
| Start recording         | `C-x (`        |
| Stop recording          | `C-x )`        |
| Execute (1st; on wards) | `C-x e`; `e`   |
| Execute \<N> times      | `C-u N C-x e`  |
| Execute EOF or error    | `C-u 0 C-x e`  |
| Execute macro \<name>   | `C-x e <name>` |

</div>

When writing macros:

- Prefer searches than movement commands.
- Break macros in parts.
- Make it reusable.
  + Look for general patters.
  + Stop recording at a position similar to start.

### Counters

We can automatically generate, and manipulate an index / counter. First, enter
the macros keymap `C-x C-k` then any of:

<div markdown="1" class="table">

| Description         | Keybinding |
|---------------------|------------|
| Insert counter      | `C-i`      |
| Counter offset \<N> | `C-c <N>`  |
| Increment by \<N>   | `C-a <N>`  |
| Set format \<f>     | `C-f <f>`  |

</div>

Where format can be `%d` for decimal, `%x` for hexadecimal, and so on.

### Macro ring

By default, emacs stores the last few macros in the macro ring for the current
session only. Again, use `C-x C-k` first, then any of:

<div markdown="1" class="table">

| Description         | Keybinding  |
|---------------------|-------------|
| Next                | `C-n`       |
| Previous            | `C-p`       |
| Execute             | `C-k`       |
| Name last \<alias>  | `n <alias>` |
| Bind last to \<key> | `b <key>`   |

</div>

Temporal macros can only be bound to keys under the `C-x C-k` keymap. We may use
any character within the 0-9, A-Z ranges as the `<key>`. Thus making the macro
temporarily available under `C-x C-k <key>`.

### Save

To reuse macros we need to add them to the `.spacemacs.d/init.el`.
Locate `defun dotspacemacs/user-config`. Insert macro with `M-x insert-kbd-macro`.

From that point on we may use them via `M-x`, `SPC SPC`, or `:`. To bind a named
macro to a keystroke we also need to add something like

```clojure
(global-set-key (kbd "C-c f") 'named-macro)
```

Being careful not to override other useful keybindings.

### Edit

<div markdown="1" class="table">

| Description  | Keybinding             |
|--------------|------------------------|
| Edit last    | `C-x C-k e RET`        |
| Edit \<name> | `C-x C-k e <name> RET` |

</div>

To be able to edit user defined macros we also need to register them under
`dotspacemacs/user-config`.

```clojure
(put 'named-macro 'kmacro t)
```

Consider writing a proper script by then, though.

Watch
[Jekor's demo on extracting links from web pages](https://www.youtube.com/watch?v=JfZ9fCHzkJw&t=5m11s){:rel="nofollow noreferrer noopener"}.

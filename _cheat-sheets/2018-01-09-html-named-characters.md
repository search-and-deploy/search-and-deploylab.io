---
description: >
  A selection of HTML Named Characters.
title: "HTML Named Characters"
---

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## Typographic Elements

<style>
  td, th { text-align:center; }
</style>

<div markdown="1" class="table">

| Character | HTML       |
|-----------|------------|
| '	     | `'`        |
| "	     | `"`        |
| &lsquo;   | `&lsquo;`  |
| &rsquo;   | `&rsquo;`  |
| &ldquo;   | `&ldquo;`  |
| &rdquo;   | `&rdquo;`  |
| &ndash;   | `&ndash;`  |
| &mdash;   | `&mdash;`  |
| &hellip;  | `&hellip;` |
| &amp;     | `&amp;`    |
| &copy;    | `&copy;`   |
| &cent;    | `&cent;`   |
| &pound;   | `&pound;`  |
| &euro;    | `&euro;`   |
| &dagger;  | `&dagger;` |
| &Dagger;  | `&Dagger;` |

</div>



## Math Elements

<div markdown="1" class="table">

| Character | HTML        | Meaning                               |
|-----------|-------------|---------------------------------------|
| &forall;  | `&forall;`  | for all                               |
| &part;    | `&part;`    | partial differential                  |
| &exist;   | `&exist;`   | there exists                          |
| &empty;   | `&empty;`   | empty set; null set                   |
| &nabla;   | `&nabla;`   | nabla; backward difference            |
| &isin;    | `&isin;`    | element of                            |
| &notin;   | `&notin;`   | not an element of                     |
| &ni;      | `&ni;`      | contains as member                    |
| &prod;    | `&prod;`    | n-ary product; product sign           |
| &sum;     | `&sum;`     | n-ary sumation                        |
| &minus;   | `&minus;`   | minus sign                            |
| &lowast;  | `&lowast;`  | asterisk operator                     |
| &radic;   | `&radic;`   | radical sign                          |
| &prop;    | `&prop;`    | proportional to                       |
| &infin;   | `&infin;`   | infinity                              |
| &ang;     | `&ang;`     | angle                                 |
| &and;     | `&and;`     | logical and; wedge                    |
| &or;      | `&or;`      | logical or; vee                       |
| &cap;     | `&cap;`     | intersection; cap                     |
| &cup;     | `&cup;`     | union; cup                            |
| &int;     | `&int;`     | integral                              |
| &there4;  | `&there4;`  | therefore                             |
| &because; | `&because;` | because                               |
| &sim;     | `&sim;`     | tilde operator; varies with           |
| &cong;    | `&cong;`    | approximately equal to                |
| &asymp;   | `&asymp;`   | almost equal to; asymptotic to        |
| &ne;      | `&ne;`      | not equal to                          |
| &equiv;   | `&equiv;`   | identical to                          |
| &le;      | `&le;`      | less-than or equal to                 |
| &ge;      | `&ge;`      | greater-than or equal to              |
| &sub;     | `&sub;`     | subset of                             |
| &sup;     | `&sup;`     | superset of                           |
| &nsub;    | `&nsub;`    | not a subset of                       |
| &sube;    | `&sube;`    | subset of or equal to                 |
| &supe;    | `&supe;`    | superset of or equal to               |
| &oplus;   | `&oplus;`   | circled plus; direct sum              |
| &otimes;  | `&otimes;`  | circled times; vector product         |
| &perp;    | `&perp;`    | up tack; orthogonal to; perpendicular |
| &sdot;    | `&sdot;`    | dot operator                          |

</div>

## Sources

- [Character Entity Reference Chart](https://dev.w3.org/html5/html-author/charref){:rel="nofollow noreferrer noopener"}
- [Toptal Character Codes](https://www.toptal.com/designers/htmlarrows/){:rel="nofollow noreferrer noopener"}
- [HTML5 Entity Names](https://www.w3schools.com/charsets/ref_html_entities_a.asp){:rel="nofollow noreferrer noopener"}
- [Named Character References](https://www.w3.org/TR/html5/syntax.html#named-character-references){:rel="nofollow noreferrer noopener"}

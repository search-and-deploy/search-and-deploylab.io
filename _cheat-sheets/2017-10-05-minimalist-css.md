---
description: >
  Minimalist responsive style guide.
title: "Minimalist CSS"
---

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## Responsive

### Media Queries
For written content, such as blogs, three screen sizes may be enough unless
end-user is expected to read it from afar.
For visually rich content four screen sizes should cater gracefully for most devices.

```css
$small:  280px; // Mobile
$medium:  768px; // Tablet
$large:  1024px; // Laptop. 960px for written content; last media query
$extra-large: 1366px; // Desktop

@media screen and (min-width: MEDIA-QUERY) { }
```

### Two Dimensional Layouts
For elements like `main` or `body` start with:

```css
display: grid;
```

CSS Tricks has a great [CSS Grid guide](https://css-tricks.com/snippets/css/complete-guide-grid/){:rel="nofollow noopener noreferrer"}

### One Dimensional
For elements such as `nav`, `header`, `aside`.

```css
display: flexbox;
```

CSS Tricks also has a great
[Flexbox guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/){:rel="nofollow noopener noreferrer"}

## Typography

### Colors
Use one color for all elements, other than links.

```css
$black: #111111;
$blue:  #0074D9;

body { color: $black; }
a { color: $blue;  }
```

### Font Sizes
There are nine HTML elements tags to give you a robust scale

```css
:root { font-size: 1rem; }
h1 { font-size: 2.5rem; }
h2 { font-size: 2.25rem; }
h3 { font-size: 2rem; }
h4 { font-size: 1.6875rem; }
h5 { font-size: 1.43rem; }
h6 { font-size: 1.2rem; }
p { font-size: 1rem; }
small { font-size: 0.875rem; }
```

To prevent fonts from growing too much lock them:

```css
@media only screen and (min-width: 48.75em) {
  body { font-size: 1.35em }
}
```

#### Responsive Fonts
Given a responsive website, responsive fonts may make sense if text is the
main content. Simply define a media query like above but considering smaller screens.

For a more precise growing ratio use the [responsive font calculator](https://websemantics.uk/tools/responsive-font-calculator/){:rel="nofollow noopener noreferrer"}.
Aim for font sizes of `1rem` to `1.375rem` (`16px` and `22px` respectively) for
good readability on all devices.

### Font Types
It is recommended to have two fonts: one for the body and one for headings.
Only add extra fonts for special use cases such as styling code.

```css
$copy:  'Roboto', 'Helvetica Neue', sans-serif;
$heading: 'Montserrat', 'Arial', sans-serif;

body { font-family: $copy; }

h1,
h2,
h3,
h4,
h5,
h6 { font-family: $heading }
```

## Styling Semantic HTML
When we use semantic HTML is easier to style elements by their role within a page.
Unfortunately, that promotes using lists of selectors to target unique elements.
Which usually leads to bugs, given the [selector specificity](https://www.w3.org/TR/selectors/#specificity){:rel="nofollow noopener noreferrer"}
rules.

Basically, style definitions with lower selector specificity override those with
a higher specificity. The basic scoring rules are:

- 1 pt. Element, and pseudo-element (eg. `:before`) selectors.
- 10 pts. Class, pseudo-class ( eg. `:root`, `:nth-child`),
attribute selectors ( ie. `[att~=val]`).
- 100 pts. Id selector: 100 pts.

Considering:

```html
<nav>
  <ul>
    <li><a href="#">Features</a></li>
    <li><a href="#">Products</a></li>
    <li><a class="btn" href="#">Sign Up</a></li>
  </ul>
</nav>
```

We may:

```css
// A.
nav ul li a.btn { }

// B.
.btn { }
```

With specificity values of 14 and 10, respectively, styling in B will override A.

## Online Tools

- [Responsive Font Calculator](https://websemantics.uk/tools/responsive-font-calculator/){:rel="nofollow noopener noreferrer"}.
- [PXtoEM](https://pxtoem.com/){:rel="nofollow noopener noreferrer"}.
- [Colors](https://clrs.cc/){:rel="nofollow noopener noreferrer"}.
- [HSLaExplorer](https://css-tricks.com/examples/HSLaExplorer/){:rel="nofollow noopener noreferrer"}.
- [W3C's Color Converter](https://www.w3schools.com/colors/colors_converter.asp){:rel="nofollow noopener noreferrer"}
- [Colors A11y](https://clrs.cc/a11y/){:rel="nofollow noopener noreferrer"}.
- [BassCss' Color Combos](https://basscss.com/v7/docs/reference/color-combinations/){:rel="nofollow noopener noreferrer"}
- [Grid Garden](https://cssgridgarden.com/){:rel="nofollow noreferrer noopener"}
- [Flexbox Froggy](https://flexboxfroggy.com/){:rel="nofollow noreferrer noopener"}
- [Specificity Calculator](https://specificity.keegan.st/){:rel="nofollow noopener noreferrer"}.

## References

- [Code Guide](https://codeguide.co/){:rel="nofollow noopener noreferrer"}.
- [CSS Grid guide](https://css-tricks.com/snippets/css/complete-guide-grid/){:rel="nofollow noopener noreferrer"}.
- [Flexbox guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/){:rel="nofollow noopener noreferrer"}.
- [CSS Purge](https://www.csspurge.com/index.html){:rel="nofollow noopener noreferrer"}.
- [Selector Specificity](https://www.w3.org/TR/selectors/#specificity){:rel="nofollow noopener noreferrer"}.
- [CSS Specificity: Things You Should Know](https://www.smashingmagazine.com/2007/07/css-specificity-things-you-should-know/){:rel="nofollow noreferrer noopener"}

---
description: >
  Basic privacy aspects according to the European Union General Data Protection
  Regulation (GDPR)
title: "GDPR Quick Reference"
---

<div markdown="1" class="table">

| Privacy Aspect      | Description                                                               | Reference                                                              |
|---------------------|---------------------------------------------------------------------------|------------------------------------------------------------------------|
| Data collection     | Only collect data for the intended purpose.                               | Art. 5 (1), 6; recitals 32- 50, 58, 60 et seq.                         |
| Children protection | Treat children related info with utmost caution.                          | Art. 6 (1) lit. f, 8, 12 (1); recitals 38, 58, 65                      |
| 3rd-party sharing   | Inform users when their personal data is shared.                          | Art. 13 (1) lit. e, 14 (1) lit. e; recitals 61                         |
| Data security       | Implement state of the art data security mechanisms.                      | Art. 32, recital 78                                                    |
| Data retention      | Determine storage periods for personal data depending on its purposes.    | Art. 17, 13 (2), 14 (2)                                                |
| Data aggregation    | Inform users when aggregated personal data is collected or shared.        | (from PrivacyCheck)                                                    |
| Data control        | Give users full control on personal data (delete, modify or transfer it). | Art. 13 (2) lit. b, 14 (2) lit. b, 15, 16, 17, 18; recitals 63 et seq. |
| Privacy settings    | Use best privacy settings. At least, allow users to modify them.          | Art. 25, recital 78                                                    |
| Account deletion    | Allow users to delete personal data at their convenience.                 | Art. 17                                                                |
| Breach notification | Inform about incident, the implications, and actions users to take.       | Art. 12, 34, 40                                                        |
| Policy changes      | Inform about policy changes in a transparent, and understandable.         | Art. 12                                                                |

</div>

## Resources

- [The Morning Paper: PrivacyGuide: towards an implementation of the EU GDPR on Internet privacy policy evaluation](https://blog.acolyer.org/2018/04/16/privacyguide-towards-an-implementation-of-the-eu-gdpr-on-internet-privacy-policy-evaluation/){:rel="nofollow noreferrer noopener"}
- [General Data Protection Regulation](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32016R0679){:rel="nofollow noreferrer noopener"}
- [PDF] [PrivacyCheck: Automatic Summarization of Privacy Policies Using Data Mining](https://identity.utexas.edu/assets/uploads/publications/Zaeem-2016-Privacy-Check.pdf){:rel="nofollow noreferrer noopener"}

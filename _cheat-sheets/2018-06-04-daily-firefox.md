---
description: >
  Firefox shorcuts, and key bindings for everyday use.
title: "Daily Firefox"
---

_**macOS**: M- == Option-_

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## Page

### Navigation

<div markdown="1" class="table">

| description       | mac kbd          |
|-------------------|------------------|
| Go back           | `Cmd-[`          |
| Go forward        | `Cmd-]`          |
| Scroll up         | &uarr;           |
| Scroll down       | &darr;           |
| Screen down       | `SPC`            |
| Screen up         | `S-SPC`          |
| Next item         | `TAB`            |
| Previous item     | `S-TAB`          |
| Open link target  | `RET`            |
| Save link target  | `M-RET`          |
| Toggle checkbox   | `SPC`            |
| Text-select mode  | `F7`             |
| Reader mode       | `Cmd-M-r`        |
| Zoom in           | `Cmd-+`          |
| Zoom out          | `Cmd--`          |
| Zoom reset        | `Cmd-0`          |
| Reload            | `Cmd-r`          |
| Refresh           | `Cmd-R`          |
| Stop load         | `ESC` or `Cmd-.` |
| Go to top         | Cmd-&uarr;       |
| Go to bottom      | Cmd-&darr;       |
| Next frame        | `F6`             |
| Previous frame    | `S-F6`           |
| Bookmarks sidebar | `Cmd-b`          |
| History sidebar   | `Cmd-H`          |

</div>


### Search

<div markdown="1" class="table">

| description   | mac kbd |
|---------------|---------|
| Find text     | `/`     |
| Find next     | `Cmd-g` |
| Find previous | `Cmd-G` |
| Find link     | `'`     |

</div>


### Media

<div markdown="1" class="table">

| description     | mac kbd    |
|-----------------|------------|
| Play / pause    | `SPC`      |
| Volume up       | &uarr;     |
| Volume down     | &darr;     |
| Mute            | Cmd-&darr; |
| Unmute          | Cmd-&uarr; |
| Rewind 15 secs  | &larr;     |
| Forward 15 secs | &rarr;     |
| Rewind 10%      | Cmd-&larr; |
| Forward 10%     | Cmd-&rarr; |

</div>


### PDF

<div markdown="1" class="table">

| description             | mac kbd    |
|-------------------------|------------|
| Open local file         | `Cmd-o`    |
| Next page               | `j` or `n` |
| Previous page           | `k` or `p` |
| Rotate clockwise        | `r`        |
| Rotate counterclockwise | `R`        |
| Presentation mode       | `Cmd-M-p`  |
| Hand tool               | `h`        |
| Text selection tool     | `s`        |
| Page number box         | `Cmd-M-g`  |

</div>

To bookmark a specific page, as well as remember the zoom factor, we need to add
a `#fragment` to the URI like so:

```plain
path/to/document.pdf#page=33&zoom=page-fit
```

The `zoom`, takes an Integer or any of `auto`, `page-fit`, and `page-width`.


## Browser


### Awesome Bar

<div markdown="1" class="table">

| description       | mac kbd     |
|-------------------|-------------|
| Go location bar   | `Cmd-l`     |
| .com autocomplete | `Cmd-RET`   |
| .net autocomplete | `S-RET`     |
| .org autocomplete | `Cmd-S-RET` |
| View in new tab   | `M-RET`     |

</div>


To limit the address bar results. Start, or finish, search queries with:

<div markdown="1" class="table">

| filter    | operator  |
|-----------|-----------|
| history   | `^`       |
| bookmarks | `*`       |
| tags      | `+`       |
| open tabs | `%`       |
| titles    | `#` __*__ |
| URLs      | `@` __*__ |

</div>

__*__ works for all stored pages


### Tabs

<div markdown="1" class="table">

| description  | mac kbd |
|--------------|---------|
| New tab      | `Cmd-t` |
| Close tab    | `Cmd-w` |
| Undo close   | `Cmd-T` |
| Next tab     | `Cmd-}` |
| Previous tab | `Cmd-{` |
| Go last tab  | `Cmd-9` |
| Quit browser | `Cmd-q`  |

</div>


### Utilities

<div markdown="1" class="table">

| description             | mac kbd |
|-------------------------|---------|
| Bookmark tab            | `Cmd-d` |
| Bookmark tabs in folder | `Cmd-D` |
| Organize bookmarks      | `Cmd-O` |
| Toggle full screen      | `Cmd-F` |
| Add-ons panel           | `Cmd-A` |
| Help                    | `Cmd-?` |

</div>

### DevTools


<div markdown="1" class="table">

| description     | mac kbd   |
|-----------------|-----------|
| Toggle DevTools | `Cmd-M-i` |
| Inspector       | `Cmd-M-c` |
| Web console     | `Cmd-M-k` |
| Style editor    | `S-F7`    |
| Profiler        | `S-F5`    |
| Network         | `Cmd-M-e` |
| Responsive view | `Cmd-M-m` |
| Page Source     | `Cmd-u`   |
| Browser console | `Cmd-J`   |

</div>


## Resources

- [Firefox Awesome Bar](https://support.mozilla.org/en-US/kb/awesome-bar-search-firefox-bookmarks-history-tabs){:rel="nofollow noreferrer noopener"}
- [Firefox Keyboard and Mouse Shortcuts](http://7is7.com/software/firefox/shortcuts.html){:rel="nofollow noreferrer noopener"}
- [Perform common Firefox tasks quickly](https://support.mozilla.org/en-US/kb/keyboard-shortcuts-perform-firefox-tasks-quickly){:rel="nofollow noreferrer noopener"}
- [DevTools All Keyboard Shortcuts](https://developer.mozilla.org/en-US/docs/Tools/Keyboard_shortcuts){:rel="nofollow noreferrer noopener"}

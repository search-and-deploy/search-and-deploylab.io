---
description: >
  Spacemacs key bindings, and snippets for the web.
title: "Spacemacs Web"
---

*Version*: 0.200.XX

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## HTML

<div markdown="1" class="table">

| Description                                               | Key Binding |
|-----------------------------------------------------------|-------------|
| highlight DOM errors                                      | `SPC m e h`   |
| go to the beginning of current element                    | `SPC m g b`   |
| go to the parent element                                  | `SPC m g p`   |
| go to next sibling                                        | `SPC m g s`   |
| show xpath of the current element                         | `SPC m h p`   |
| delete the current element (does not delete the children) | `SPC m r d`   |
| rename current element                                    | `SPC m r r`   |
| wrap current element                                      | `SPC m r w`   |
| fold/unfold current element                               | `SPC m z`   |

</div>

Initiate transient-state with `SPC m .` or `~,.~` to do:

<div markdown="1" class="table">

| Description                                                    | Key Binding |
|----------------------------------------------------------------|-------------|
| leave the transient-state                                      | `q`         |
| Toggle full help                                               | `?`         |
| clone current element                                          | `c`         |
| delete (vanish) current element (does not delete the children) | `d`         |
| delete current element and children                            | `D`         |
| next element                                                   | `j`         |
| next sibling element                                           | `J` / `gj`  |
| parent element                                                 | `h`         |
| previous element                                               | `k`         |
| previous sibling element                                       | `K` / `gk`  |
| first child element                                            | `l`         |
| show xpath of current element                                  | `p`         |
| rename current element                                         | `r`         |
| wrap current element                                           | `w`         |

</div>

## CSS

For SCSS linting `gem install` [scss_lint_reporter_checkstyle](https://github.com/Sweetchuck/scss_lint_reporter_checkstyle){:rel="nofollow noreferrer noopener"}
and [scss-lint](https://github.com/brigade/scss-lint){:rel="nofollow noreferrer noopener"}

<div markdown="1" class="table">

| Description                           | Key Binding |
|---------------------------------------|-------------|
| quickly navigate CSS rules using helm | `SPC m g h`   |
| fold css statement to one line        | `SPC m z c`   |
| unfold css statement to one line      | `SPC m z o`   |

</div>


## Code Expansion

The `html` layer also installs emmet. To expand emmet formulas simply press `C-j`
at the end.

<div markdown="1" class="table">

| Description            | Key Binding   |
|------------------------|---------------|
| Prefix Command         | `C-c`         |
| emmet-expand-line      | `C-j`/`C-RET` |
| emmet-prev-edit-point  | `C-M-left`    |
| emmet-next-edit-point  | `C-M-right`   |
| emmet-wrap-with-markup | `C-c w`       |

</div>


### HTML abbreviations

#### Basic tags

<div markdown="1" class="table">

| HTML                                   | Emmet       |
|----------------------------------------|-------------|
| `<a href=""></a>`                      | `a`         |
| `<a id="q" class="x y z" href=""></a>` | `a#q.x.y.z` |
| `<div id="q"></div>`                   | `#q`        |
| `<div class="x"></div>`                | `.x`        |

</div>

#### Self-closing tags

<div markdown="1" class="table">

| HTML                                    | Emmet              |
|-----------------------------------------|--------------------|
| `<input type="text" name="" value=""/>` | `input[type=text]` |
| `<img src="" alt=""/>`                  | `img`              |
| `<meta id="q" class="x y z" href=""/>`  | `meta/#q.x.y.z`    |

</div>

#### Siblings

<div markdown="1" class="table">

| HTML                     | Emmet |
|--------------------------|-------|
| `<a href=""></a><b></b>` | `a+b` |

</div>

We can now concat `a#q.x.y.z+b#p.l.m.n` to get:

```html
<a id="q" class="x y z" href=""></a>
<b id="p" class="l m n"></b>
```

#### Tag expansion

`table+` generates:

```html
<table>
  <tr>
    <td>
    </td>
  </tr>
</table>
```

`dl+` expands to:

```html
<dl>
  <dt></dt>
  <dd></dd>
</dl>
```

Furthermore, `ul++ol+` produces:

```html
<ul>
  <li></li>
</ul>
<ol>
  <li></li>
</ol>
```

Also, `ul#q.x.y[m=l]` results in:

```html
<ul id="q" class="x y" m="l">
  <li></li>
</ul>
```

#### Parent > child

For nested element, `a#q.x.y.z>b#p.l.m.n` would generate:

```html
<a id="q" class="x y z" href="">
  <b id="p" class="l m n"></b>
</a>
```

And `a>b+c>d` creates:

```html
<a href="">
  <b></b>
  <c>
    <d></d>
  </c>
</a>
```

#### Multiplication

We can add repeated element using multiplication `a*2>b*2`

```html
<a href="">
  <b></b>
  <b></b>
</a>
<a href="">
  <b></b>
  <b></b>
</a>
```

Multiplication applies to properties `a#q.x>b#q.x*2`. Beware of `id`s.

```html
<a id="q" class="x" href="">
  <b id="q" class="x"></b>
  <b id="q" class="x"></b>
</a>
```

#### Properties

<div markdown="1" class="table">

| HTML                                          | Emmet                    |
|-----------------------------------------------|--------------------------|
| `<b x=""></b>`                                | `b[x]`                   |
| `<b x="y"></b>`                               | `b[x=y]`                 |
| `<b x="y"></b>`                               | `b[x="y"]`               |
| `<b x="()"></b>`                              | `b[x="()"]`              |
| `<b x="" m=""></b>`                           | `b[x m]`                 |
| `<b id="foo" class="bar mu" x="y" m="l"></b>` | `b#foo.bar.mu[x=y m=l]`  |
| `<b id="foo" class="bar mu" x="y" m="l"/>`    | `b/#foo.bar.mu[x=y m=l]` |

</div>

#### Text

We can use curly braces like so `p>{Click}+span{here}+{ to continue}` for
embedded text:

```html
<p>
  Click
  <span>here</span>
  to continue
</p>
```

### CSS abbreviations

#### Basic Usage

Emmet formulas like `p1-2!+m10e+bd1#2s` produce CSS like

```css
padding: 1px 2px !important;
margin: 10em;
border: 1px #222 solid;
```

#### Keywords

<div markdown="1" class="table">

| CSS                                     | Emmet  |
|-----------------------------------------|--------|
| `margin: ;`                             | `m`    |
| `background: #fff url() 0 0 no-repeat;` | `bg+`  |
| `color: #000;`                          | `c#0`  |
| `width`                                 | `w`    |
| `height`                                | `h`    |
| `background`                            | `bg`   |
| `font-weight: bold;`                    | `fw:b` |

</div>


#### Values

Separate each value either with `-` or spaces.

<div markdown="1" class="table">

| CSS                                                      | Emmet                            |
|----------------------------------------------------------|----------------------------------|
| `margin: 1px 2px 3px 4px;`                               | `m1-2-3-4`                       |
| `margin: 1px 2px 3px 4px;`                               | `m1 2 3 4`                       |
| `margin: 1px -2px;`                                      | `m1--2`                          |
| `margin: 1px -2px;`                                      | `m1 -2`                          |
| `background: #cccccc url(/back.png) 10px 20px repeat-x;` | `bg+#c /back.png 10 20 repeat-x` |

</div>


#### Multiple property definition

Concatenate properties using `+`. For instance, `m0+p10+c#f+fw:b+w100+h20+bg#f00`
results in:

```css
margin: 0px;
padding: 10px;
color: #fff;
font-weight: bold;
width: 100px;
height: 20px;
background: #f00;
```

#### Value units

<div markdown="1" class="table">

| CSS                     | Emmet       |
|-------------------------|-------------|
| `margin: 10% 30em 5ex;` | `m10p30e5x` |

</div>

| Unit  | Alias     |
|-------|-----------|
| `px`  | (nothing) |
| `em`  | `e`       |
| `%`   | `p`       |
| `rem` | `r`       |

## Resources

- [Emmet Official Cheat Sheet](https://docs.emmet.io/cheat-sheet/){:rel="nofollow noreferrer noopener"}
- [Emmet CSS Abbreviations](https://docs.emmet.io/css-abbreviations/){:rel="nofollow noreferrer noopener"}

---
description: > # "title:"
  Key web typography rules.
title: "Web Typography"
---

The typographic quality of a document is largely determined by how the body text
looks. Keep the following points in mind for a better body text.

- Point size. 15-25 pixels.+
- Line spacing. `line-height: 1.5`. 120-145% of the point size.
- Line length. `p { max-width: 66ch; }` ideal. 45 to 85 characters is good.
- System Fonts. Avoid using Times New Roman, and Arial.
- Monospaced Fonts. Only use them for code.
- Serif fonts. Only on higher resolution screens.
- Word spaces. Never use them for styling. Use `&thinsp;` for acronyms.
- Underlining. Don't use it unless it's a hyperlink.
- Bold and Italics. Use as little as possible.
- Small caps. Only if actually available. Use an extra 5-12% of letter spacing.
- First-line indents or paragraph spaces, never both.
- Fist-line indent. 1-4x the text point size.
- Paragraph space. 4-10 points of space between paragraphs.
- Use hyphenation in justified text
- Use curly quotes.

Note +: WebAIM defines large font as ~18.66px (14 point), but says, implying
context, that ~24px (18 point), and larger are ok.

## Typographic Elements

`M-` = Option + \<key>

<style>
  td, th { text-align:center; }
</style>

<div markdown="1" class="table">

| Character | OS X  | HTML       |
|-----------|-------|------------|
| '	     | `'`   | `'`        |
| "	     | `"`   | `"`        |
| &lsquo;   | `M-]` | `&lsquo;`  |
| &rsquo;   | `M-}` | `&rsquo;`  |
| &ldquo;   | `M-[` | `&ldquo;`  |
| &rdquo;   | `M-{` | `&rdquo;`  |
| &ndash;   | `M--` | `&ndash;`  |
| &mdash;   | `M-_` | `&mdash;`  |
| &hellip;  | `M-;` | `&hellip;` |
| &amp;     | &     | `&amp;`    |
| &copy;    | `M-g` | `&copy;`   |
| &cent;    | `M-4` | `&cent;`   |
| &pound;   | `M-3` | `&pound;`  |
| &euro;    | `M-@` | `&euro;`   |

</div>


## Resources

For more elements check out:

- [HTML5 Character Entity Reference Chart](https://dev.w3.org/html5/html-author/charref){:rel="nofollow noopener noreferrer"}
- [HTML5 Entity Tables](https://www.tutorialspoint.com/html5/html5_entities.htm){:rel="nofollow noopener noreferrer"}
- [Typewolf's Typography Cheatsheet](https://www.typewolf.com/cheatsheet){:rel="nofollow noopener noreferrer"}
- [HTML Symbols](https://www.w3schools.com/charsets/ref_utf_math.asp){:rel="nofollow noopener noreferrer"}
- [Contrast Explanation](https://webaim.org/resources/contrastchecker){:rel="nofollow noreferrer noopener"}

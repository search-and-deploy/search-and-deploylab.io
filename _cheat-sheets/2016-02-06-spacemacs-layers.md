---
description: >
  Extra key bindings for different Spacemacs layers.
title: "Spacemacs layers"
---

*Version*: 0.200.XX

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## [Enh-]Ruby

<div markdown="1" class="table">

| Description                       | Key Binding |
|-----------------------------------|-------------|
| Change " to '                     | `SPC m x '` |
| Change ' to "                     | `SPC m x "` |
| Change string to symbol           | `SPC m x :` |
| evil-matchit jumps between blocks | `%`         |

</div>

**Note:** For a better experience using the Ruby layer may add `minitest`, and
`reek` gems to the local system. Also, use `rbenv` or similar.

### REPL

Make sure to have a `Gemfile` in your project and include `gem 'pry'`.

<div markdown="1" class="table">

| Description                                 | Key Binding |
|---------------------------------------------|-------------|
| start REPL                                  | `SPC m s i` |
| switch to REPL                              | `SPC m s s` |
| go to definition (robe-jump)                | `SPC m g g` |
| go to Documentation                         | `SPC m h d` |
| send function definition                    | `SPC m s f` |
| send function definition and switch to REPL | `SPC m s F` |
| send region                                 | `SPC m s r` |
| send region and switch to REPL              | `SPC m s R` |

</div>

### Test Mode

<div markdown="1" class="table">

| Description         | Key Binding |
|---------------------|-------------|
| run test file       | `SPC m t b` |
| run test at pointer | `SPC m t t` |

</div>


## Shell

<div markdown="1" class="table">

| Description                    | Key Binding |
|--------------------------------|-------------|
| Open/close shell               | `SPC '`     |
| Open/close zsh                 | `SPC a s T` |
| Browse command history         | `SPC m H`   |
| Next item in history           | `C-j`       |
| Previous item in history       | `C-k`       |
| Create \<N> shells. Go to \<N> | `<N> SPC '` |

</div>


## Org Mode

_`M-RET` and `SPC m` are equivalent in org-mode_

### Editing

<div markdown="1" class="table">

| Description               | Keybinding  |
|---------------------------|-------------|
| insert heading            | `M-RET h i` |
| insert subheading         | `M-RET h s` |
| insert footnote           | `M-RET i f` |
| insert link interactively | `M-RET i l` |
| insert `TODO` heading     | `T`         |
| show export options       | `C-c C-e`   |

</div>

### Sections

A section is delimited by another section's heading. Same applies to
subsections.

<div markdown="1" class="table">

| Description                 | Keybinding             |
|-----------------------------|------------------------|
| navigate between headings   | `gh`, `gj`, `gk`, `gl` |
| un-indent line              | `<`                    |
| indent line                 | `>`                    |
| promote subsection          | `M-h`                  |
| demote subsection           | `M-l`                  |
| move subsection up          | `M-k`                  |
| move subsection down        | `M-j`                  |
| mark last heading as `TODO` | `t`                    |

</div>

#### Visibility

<div markdown="1" class="table">

| Description               | Keybinding             |
|---------------------------|------------------------|
| toggle current subsection | `TAB`                  |
| toggle all sections       | `S-TAB`                |
| focus on subtree          | `M-RET n`              |
| focus off                 | `M-RET N`              |

</div>

### Lists

_Note: `<sideways>` stands for either left or right arrows_

<div markdown="1" class="table">

| Description    | Keybinding     |
|----------------|----------------|
| toggle bullets | `S-<sideways>` |
| demote item    | `M-l`          |
| promote item   | `M-h`          |
| move up        | `M-k`          |
| move down      | `M-j`          |

</div>


### Tables

<div markdown="1" class="table">

| Description          | Keybinding    |
|----------------------|---------------|
| previous cell        | `(`           |
| next cell            | `)`           |
| beginning of table   | `{`           |
| end of table         | `}`           |
| move column sideways | `M-h` / `M-l` |
| move row up / down   | `M-k` / `M-j` |
| select cell          | `vae`         |
| select row           | `vaE`         |
| select table         | `var`         |
| prepend column       | `M-L`         |
| prepend row          | `M-J`         |
| delete column        | `M-H`         |
| delete row           | `M-K`         |
| continue numbering   | `S-RET`       |

</div>

## ERC

<div markdown="1" class="table">

| Description          | Key Binding |
|----------------------|-------------|
| IRC                  | `SPC a i e` |
| Gitter (IRC tsl/ssl) | `SPC a i E` |
| Next buffer          | `SPC a i i` |
| Switch buffers       | `SPC m b`   |
| Send action          | `SPC m d`   |
| Join channel         | `SPC m j`   |
| Run `/names#channel` | `SPC m n`   |
| Run `/list`          | `SPC m l`   |
| Part channel         | `SPC m p`   |
| Quit ERC             | `SPC m q`   |

</div>


## Magit

<div markdown="1" class="table">

| Description            | Key Binding |
|------------------------|-------------|
| list magit commands    | `SPC g m`   |
| show git status        | `SPC g s`   |
| open magit blame       | `SPC g b`   |
| start git time machine | `SPC g t`   |

</div>

### Magit buffer

Many of Magit's commands popup a buffer for an interactive user experience. These
can only be used in such buffers.

<div markdown="1" class="table">

| Description             | Key Binding |
|-------------------------|-------------|
| display help            | `C-c C-c`   |
| log current branch      | `l l`       |
| diff staged changes     | `d s`       |
| diff unstaged changes   | `d u`       |
| stage file or change    | `s`         |
| unstage file or change  | `u`         |
| create new commit       | `c c`       |
| amend commit            | `c a`       |
| rebase elsewhere \*     | `r e`       |
| rebase interactive      | `r -i e`    |
| checkout a branch       | `b b`       |
| create branch           | `b c`       |
| fetch changes           | `f f`       |
| pull and rebase tracked | `F (r) u`   |
| refresh                 | `g r`       |

</div>

<small markdown="1">
_\* Normally on `master`_
</small>

Check out Guowei Lv's tutorials on
[rebase basics](https://www.lvguowei.me/post/magit-rebase/){:rel="nofollow noreferrer noopener"},
and [rebase before merging branches](https://www.lvguowei.me/post/magit-rebase-2/){:rel="nofollow noreferrer noopener"}
on Magit. Also, check out his
[tutorial on Magit bisect](https://www.lvguowei.me/post/magit-tutorial-bisect/){:rel="nofollow noreferrer noopener"}.

## Resources

- [Org-mode](https://orgmode.org/){:rel="nofollow noreferrer noopener"}
- [Org-mode Cheat Sheet](https://orgmode.org/orgcard.pdf){:rel="nofollow noreferrer noopener"}
- [Evil Org-mode](https://github.com/Somelauw/evil-org-mode){:rel="nofollow noreferrer noopener"}
- [Evil Org-mode Full Overview](https://github.com/Somelauw/evil-org-mode/blob/master/doc/keythemes.org){:rel="nofollow noreferrer noopener"}

---
description: >
  Basic Spacemacs keybindings. How to exit. File management. Moving the cursor.
  The buffer. Edit mode basics. Insert mode basics. Windows. UI toggles. Updates.
title: "Spacemacs Basics"
---

*Version*: 0.200.XX

<details markdown="1" id="table-of-contents">
  <summary>
    Table of Contents
  </summary>

  * TOC
  {:toc}
</details>

## Keybinding mode

`C-z` toggles holy and evil keybinding modes. When the cursor is blue, spacemacs
is in holy mode. When it's yellow, in evil. All spacemacs cheat sheets assume evil
mode.

To get a list of all the keybindings type `SPC ?`. Check the
[section below on Helm](#helm){:rel="nofollow noreferrer noopener"}
for details on using that kind of list.

## Exit Spacemacs

<div markdown="1" class="table">

| Description              | Keybinding |
|--------------------------|------------|
| stop / cancel            | `C-g`      |
| restart spacemacs        | `SPC q r`  |
| exit spacemacs           | `C-x C-c`  |
| save then exit spacemacs | `:wqa RET` |
| close/exit mode buffers  | `q`        |

</div>


## File Management

### Helm

First we decide what we want to do:

<div markdown="1" class="table">

| Description               | Keybinding |
|---------------------------|------------|
| open file                 | `SPC f f`  |
| save file                 | `SPC f s`  |
| recently open files       | `SPC f r`  |
| rename current file       | `SPC f R`  |
| delete current file       | `SPC f D`  |
| copy current file         | `SPC f c`  |
| open binary in hex editor | `SPC f h`  |
| list file's dir contents  | `SPC f j`  |

</div>

<small markdown="1">
_The `SPC ff` keybinding can also take a path, in which case it will create all
necessary directories in between._
</small>

Then, we choose on which file we'll do it. To move around the list

<div markdown="1" class="table">

| Description           | Keybinding |
|-----------------------|-------------|
| go up one directory   | `C-h`       |
| move up/down the list | `C-k`/`C-j` |
| jump to file          | `C-q`       |
| Completion            | `TAB`       |

</div>

At times we might lose focus on Helm's mini-buffer. To regain focus simply
`SPC w b`.

#### Projects

Spacemacs automatically tracks any git project (`.git` sub-directory), whose file
we open.

<div markdown="1" class="table">

| Description             | Keybinding |
|-------------------------|------------|
| List projects           | `SPC p p`  |
| Find a file in project  | `SPC p f`  |
| List project open files | `SPC p h`  |

</div>

`RET` completes the actions started with either of the `SPC f` keybinding family.
It also opens the highlighted project/folder.


### NeoTree

<div markdown="1" class="table">

| Description                | Keybinding  |
|----------------------------|--------------|
| toggle NeoTree at pwd      | `SPC f t`    |
| toggle NeoTree at root     | `SPC p t`    |
| hide NeoTree buffer        | `q` or `fd`  |
| stretch tree               | `TAB`        |
| navigate files/dir         | `j` / `k`    |
| make root directory        | `R`          |
| expand dir; open file      | `l` or `RET` |
| open file in window \<#>   | `<#> l`      |
| open file vertical split   | `¦`          |
| open file horizontal split | `-`          |
| toggle hidden files        | `s`          |
| create node                | `c`          |
| rename node                | `r`          |
| delete node                | `d`          |
| help                       | `?`          |

</div>

### Crash recovery

Spacemacs auto-saves files. To recover them simply `SPC SPC recover` to
list all recovery options.

## Moving the cursor

<div markdown="1" class="table">

| Description          | Keybinding                     |
|----------------------|---------------------------------|
| One line down        | `j`                             |
| One line up          | `k`                             |
| left                 | `h`                             |
| right                | `l`                             |
| back one word        | `b`                             |
| advance one word     | `w`                             |
| beginning of line    | `0`                             |
| end of line          | `$`                             |
| Beginning of file    | `g g`                           |
| End of file          | `G`                             |
| Top of buffer        | `H`                             |
| Middle of buffer     | `M`                             |
| End of buffer        | `L`                             |
| Go to line \<number> | `:<number> RET` or `<number> G` |
| Jump to line         | `SPC j l <line-anchor>`         |
| Jump to \<char>      | `SPC j j <char>`                |

</div>

### Marks

We can set any character as a marker (`<m>`). That is, a temporal
"buffer bookmark". Marks allow us to move around a document easily.

<div markdown="1" class="table">

| Description             | Keybinding |
|-------------------------|------------|
| set marker to `<c>` key | `m <c>`    |
| back to marker `<c>`    | `' <c>`    |
| back to elsewhere       | `' '`      |
| view all marks          | `:marks`   |
| delete marks            | `:delm`    |

</div>

Since markers only last for a session, all marks are cleared when we close its
buffer.


## Buffer

<div markdown="1" class="table">

| Description                   | Keybinding |
|-------------------------------|------------|
| Scroll down                   | `C-e`      |
| Scroll up                     | `C-y`      |
| Page up                       | `C-u`      |
| Page down                     | `C-d`      |
| Swap between last two buffer  | `SPC TAB`  |
| Close current buffer          | `SPC b d`  |
| Pick buffer to close          | `C-x k`    |
| Switch buffer                 | `SPC b b`  |
| Scroll full screen down       | `C-b`      |
| Scroll full screen up         | `C-f`      |
| Scroll current line to top    | `zt`       |
| Scroll current line to middle | `zz`       |

</div>

<small markdown="1">
_The `SPC bb` keybinding can take a filename/buffer to search for it. `RET` will
open it._
</small>

## Basic editing

_AKA Command mode_

<small markdown="1">
  *Note:* _`<matched>` may be any char of the pairs `""`, `''`, `()`, `{}`, `[]`_
</small>


<div markdown="1" class="table">

| Description                        | Keybinding      |
|------------------------------------|-----------------|
| Copy line                          | `y y`           |
| Copy word                          | `y w`           |
| Delete character under cursor      | `x`             |
| Delete a word                      | `d a w`         |
| Replace word                       | `c i w`         |
| Delete from cursor to end of word  | `d w`           |
| Delete from cursor to end of line  | `d $`           |
| Delete a whole line                | `d d`           |
| Indent line                        | `> >`           |
| Unindent line                      | `< <`           |
| Remove paragraph indentation       | `= a p`         |
| Undo last command                  | `u`             |
| Redo command                       | `C-r`           |
| Repeat last command                | `.`             |
| Put last deletion after cursor     | `p`             |
| Replace `<matched>`  contents      | `c i <matched>` |
| Join lines separated by whitespace | `J`             |
| List copied (yanked) text\*        | `SPC r y`       |
| Delete `<matched>` w/ contents     | `d a <matched>` |
| Delete `<matched>` contents        | `d i <matched>` |

</div>

<small markdown="1">
_\*We can move around the list of yanked text with the Helm keybindings. `RET` pastes
the highlighted line under wherever the cursor was._
</small>

Many of the commands above can be preceded by a number to get the command that
amount of times. For instance, `3dd` will delete the line under the cursor, as
well the two below it.

## Insert mode basics

You can type after any of these as in a regular editor.

<div markdown="1" class="table">

| Description            | Keybinding |
|------------------------|-------------|
| Insert (before cursor) | `i`         |
| Append (after cursor)  | `a`         |
| Append (end of line)   | `A`         |
| Prepend (to line)      | `I`         |
| New open line below    | `o`         |
| New open line above    | `O`         |
| Correct word           | `c w`       |
| Correct line           | `c c`       |
| Exit insert mode       | `ESC`       |

To toggle from `insert mode` to `command mode` quickly we can use `ESC` or type
`fd` quickly.

</div>

## Window

<div markdown="1" class="table">

| Description               | Keybinding |
|---------------------------|-------------|
| Split window to the right | `SPC w V`   |
| Split window horizontally | `SPC w S`   |
| Close current window      | `SPC w d`   |
| Rotate windows            | `SPC w r`   |
| Toggle golden ratio       | `SPC t g`   |
| Switch to window \<#>     | `SPC <#>`   |
| Close window              | `SPC w c`   |

</div>

## UI Toggles

Most of these key binding toggle on/off different
'UI' elements. All are helpful at one time or another
but hardly ever all at once.

<div markdown="1" class="table">

| Description                          | Keybinding  |
|--------------------------------------|-------------|
| Highlight current line               | `SPC t h h` |
| Display indentation guide            | `SPC t i`   |
| Display absolute line numbers        | `SPC t n`   |
| Version control info                 | `SPC t m v` |
| Global indent guide                  | `SPC t TAB` |
| Display fill column                  | `SPC t f`   |
| Display whitespace                   | `SPC t w`   |
| Highlight indentation current column | `SPC t h c` |
| Highlight indentation level          | `SPC t h i` |
| Fringe (~)                           | `SPC t ~`   |
| Display fringe                       | `SPC T f`   |
| Cycle color theme                    | `SPC T n`   |

</div>

## Init file

| Description                | Keybinding  |
|----------------------------|-------------|
| Go to init.el              | `SPC f e d` |
| Apply changes              | `SPC f e R` |
| Diff init.el after upgrade | `SPC f e D` |

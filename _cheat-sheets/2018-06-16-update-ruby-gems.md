---
description: >
  Basic Ruby gems maintenance flows.
title: "Update Ruby gems"
---

Ruby gems can be broadly divided into

- [System gems](#system-gems).
CLIs, linters, scripts, an so on, meant to be run as part of the system.
- [Library gems](#library-gems).
Libraries, toolbox, and frameworks meant to be used in code.

## System gems

Given a local box with Ruby's
[default, and bundle gems](https://idiosyncratic-ruby.com/20-better-standards.html#default-gems){:rel="nofollow noreferrer noopener"},
as well as a few system gems.

```terminal
$ gem outdated
$ gem update
$ gem cleanup -d
```

The `-d` flag, in the last command, stands for dry run. Which invokes the command
without removing any old gem merely to show what we can expect if we run it without
the flag.

### Rubygems

To update Rubygems, as in the CLI, to the latest version

```terminal
$ gem update --system
```

## Library gems

Nowadays, Ruby projects are expected to manage dependencies with bundler, via a
`Gemfile` (from 2.5, it could be `gems.rb` instead). Assuming the `rake` command
runs all tests, we can update a project's dependencies with:

```shell
$ rake

# update gem version in Gemfile

$ bundle install
$ rake
```

To prevent introducing bugs massively due to broken APIs, or any other conflicts,
upgrade each gem individually. Occasionally group upgrade is needed ie. gem with
plugins. Once done upgrading all new gems:

```shell
$ bundle clean --dry-run
$ bundle clean
```

Consider subscribing to the RSS feed of a project's main gems to keep up-to-date
with security patches, and other fixes. We can subscribe to any ruby gem feed with
the formula:

```text
https://rubygems.org/gems/<gem_name>/versions.atom
```

## Resources

- [Gem Command Reference](https://guides.rubygems.org/command-reference){:rel="nofollow noreferrer noopener"}
- [Bundler Docs](https://bundler.io/docs.html){:rel="nofollow noreferrer noopener"}

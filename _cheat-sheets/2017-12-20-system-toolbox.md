---
description: >
  Tips, and tricks covering builtins, and other common CLIs.
title: "System Toolbox"
---

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>

## Help

<div markdown="1" class="table">

| Description                   | Command              |
|-------------------------------|----------------------|
| \<cmd>'s usage                | `<cmd> --help`       |
| Get info on \<cmd>            | `info <cmd>`         |
| Show \<cmd>'s manual          | `man <cmd>`          |
| \<cmd> summary                | `whatis <cmd>`       |
| Search command by \<keywords> | `apropos <keywords>` |
| Query the type of \<cmd>      | `type <cmd>`         |
| Binary used on \<cmd>         | `which <cmd>`        |

</div>

## Display

<div markdown="1" class="table">

| Description                       | Command          |
|-----------------------------------|------------------|
| Clear screen                      | `clear`          |
| Reset terminal display            | `reset`          |

</div>


## Directory management

<div markdown="1" class="table">

| Description                 | Command                         |
|-----------------------------|---------------------------------|
| Current directory           | `pwd`                           |
| Go to directory             | `cd [directory]`                |
| Go back a directory         | `cd ..`                         |
| Go back 2 directories       | `cd ../../`                     |
| Go `HOME`                   | `cd` or `cd ~`                  |
| Go to root                  | `cd /`                          |
| Last directory opened       | `cd -`                          |
| New directory               | `mkdir [directory]`             |
| New path                    | `mkdir -p [nested/directories]` |
| Remove empty directory      | `rmdir [directory]`             |
| Remove directory & contents | `rm -r [directory]`             |

</div>


### Directory stack

_pushd and popd work according to the LIFO principle_

<div markdown="1" class="table">

| Description                    | Command       |
|--------------------------------|---------------|
| Show stack                     | `dirs`        |
| Show indexed stack             | `dirs -v`     |
| Clear stack                    | `dirs -c`     |
| Add `<dir>` to top and go      | `pushd <dir>` |
| Only add `<dir>` to top        | `pushd -n`    |
| Toggle top two dirs            | `pushd`       |
| Remove top dir; go to new top  | `popd`        |
| Only remove top dir            | `popd -n`     |
| Go dir index `<I>` from top    | `pushd +<I>`  |
| Go dir index `<I>` from bottom | `pushd -<I>`  |

</div>


## File management

<div markdown="1" class="table">

| Description                    | Command                     |
|--------------------------------|-----------------------------|
| List files                     | `ls`                        |
| List files incl. hidden        | `ls -a`                     |
| List files (human friendly)    | `ls -lh`                    |
| New file                       | `touch [file]`              |
| View file                      | `less [file]`               |
| Remove a file                  | `rm [file]`                 |
| Force removal w/o confirmation | `rm -f [file]`              |
| Copy file to new file          | `cp [file] [newfile]`       |
| Copy file to directory         | `cp [file] [dir]`           |
| Move/Rename                    | `mv [file] [new path/name]` |
| New file from command output   | `[command] > [file]`        |
| Append output to file          | `[command] >> [file]`       |
| Read file contents w/command   | `[command] < [file]`        |

</div>

### Search

<div markdown="1" class="table">

| Description                         | Command                         |
|-------------------------------------|---------------------------------|
| Search for filename in /some/dir    | `find /some/dir -name "file"`   |
| Lines in file with pattern          | `grep "<pattern>" file`         |
| Lines in dir files with pattern     | `grep -r "<pattern>" /some/dir` |
| Lines missing the pattern           | `grep -v "<pattern>" file`      |
| Lines with case-insensitive pattern | `grep -i "<pattern>" file`      |

</div>

## Command chains

<div markdown="1" class="table">

| Description                     | Command              |
|---------------------------------|----------------------|
| Run command A, then B           | `[cmd-A]; [cmd-B]`   |
| Run command B if A succeeded    | `[cmd-A] && [cmd-B]` |
| Run command B if A failed       | `[cmd-A] || [cmd-B]` |
| Run command A, pass result to B | `[cmd-A] | [cmd-B]`  |

</div>


## Command history

<div markdown="1" class="table">

| Description                           | Command      |
|---------------------------------------|--------------|
| Show last n commands                  | `history n`  |
| Show last command that starts w/value | `![value]:p` |
| Run last command that starts w/value  | `![value]`   |
| Run the last command                  | `!!`         |
| Show the last command                 | `!!:p`       |

</div>

Prepend any command with a space to exclude it from history.

```terminal
$  sensitive_cmd
```

## Jobs

<div markdown="1" class="table">

| Description                      | Command/Keybinding |
|----------------------------------|--------------------|
| Show running jobs                | `jobs`             |
| Run \<cmd> in background         | `<cmd> &`          |
| Suspend job/process              | `C-z`              |
| Bring last job to foreground     | `fg`               |
| Bring job \<id> to foreground    | `%<id>`            |
| Resume job \<id> in background   | `bg %<id>`         |
| End process                      | `C-c`              |
| EOF running process; exit shell. | `C-d`              |

</div>


## SSH key creation

We can create SSH keys using `ssh-keygen`.

```terminal
$ ssh-keygen -f ~/.ssh/id_ed25519 -t ed25519 -C "public_email@example.com"
```

For legacy systems where `ed25519` isn't available: (avoid whenever possible!)

```terminal
$ ssh-keygen -f ~/.ssh/id_rsa -t rsa -b 4096 -o -a 100 -C "public_email@example.com"
```

If more than one set of keys are needed consider `~/.ssh/id_ed25519_<profile>`
as a naming format. Also, add `-C "descriptive comment"`.

To copy the new public key to the clipboard:

```terminal
$ pbcopy < ~/.ssh/id_ed25519.pub
```

On systems with `xclip` rather than `pbcopy` we can:

```shell
alias pbcopy='xclip -selection clipboard -in'
```

Use a `config` file to ease key management. Also, consider adding custom
git details from `.gitconfig` file.


## Permissions

Permission is usually handle by the `ch*` family of commands.

On the terminal, `ls -l` outputs something like:

```terminal
drwxr-xr-x 2 pete penguins 4096 Dec 1 11:45 .
```

where

<div markdown="1" class="table">

| type | owner | group | other |
| d    | rwx   | r-x   | r-x   |

</div>

- `d`. Directory. If `-`, then is a file.
- `r`. Readable. Value 4.
- `w`. Writable. Value 2.
- `x`. Executable. Value 1.
- `-`. Empty ie. permission off.

<div markdown="1" class="table">

| who       | r | w | x | mode |
|-----------|---|---|---|------|
| owner (u) | t | t | f |    6 |
| group (g) | t | f | f |    4 |
| other (o) | t | f | f |    4 |

</div>

Owner is also known as 'user' to avoid being confused with 'other'.


### Change Permissions

allow user to exec `<file>`

```terminal
$ chmod u+x <file>
```

disallow group from writing on `<file>`

```terminal
$ chmod g-w <file>
```

allow user, and group to write on `<file>`

```terminal
$ chmod ug+w <file>
```

modify `<file>` permission to match code

```terminal
$ chmod 755 <file>
```

transfer `<file>` ownership to `<user2>`

```terminal
$ sudo chown <user2> <file>
```

set `<file>`'s owner group

```terminal
$ sudo chgrp <group2> <file>
```

set `<file>` owners to `<user2>` and `<group2>`

```terminal
$ sudo chown <user2>:<group2> <file>
```

### Sticky bit

This permission sticks files or directories, that is, prevents others
from deleting and/or modifying them.

```terminal
$ sudo chmod +t <dir>
# or
$ sudo chmod 1755 <file>
```

There are other special bits concerning SUID, and SGID which are mostly
use to securely grant special permissions to non-root users in shared
environments.

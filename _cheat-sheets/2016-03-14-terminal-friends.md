---
description: >
  Key bindings for terminal emulators, and some of their
  common tools.
title: "Terminal friends"
---

<details markdown="1" id="table-of-contents">
<summary>
Table of Contents
</summary>

* TOC
{:toc}
</details>


## iTerm2 (macOS)

Cmd = ⌘ key

<div markdown="1" class="table">

| Description                             | Key Binding         |
|-----------------------------------------|---------------------|
| Move cursor to beginning of line        | `C-a`               |
| Move cursor to end of line              | `C-e`               |
| Move cursor beginning of word           | `ESC + b`           |
| Move cursor end of word                 | `ESC + f`           |
| Delete word after cursor                | `ESC + d`           |
| Delete word behind cursor               | `C-w`               |
| Delete from cursor to beginning of line | `C-u`               |
| Delete from cursor to end of line       | `C-k`               |
| Delete character under cursor           | `C-d`               |
| New tab                                 | `Cmd-t`             |
| Switch to tab at \<left-right>          | `Cmd-<left-right>`  |
| Add vertical pane                       | `Cmd-d`             |
| Add horizontal pane                     | `Cmd-D`             |
| Switch to pane at \<direction>          | `M-Cmd-<direction>` |
| Close pane                              | `Cmd-w`             |

</div>

## Nano

<div markdown="1" class="table">

| Description          | Key Binding |
|----------------------|-------------|
| Next page            | `C-v`       |
| Previous page        | `C-y`       |
| Last page            | `C-w-v`     |
| Store line in buffer | `C-k`       |
| Reinsert line        | `C-u`       |
| Exit                 | `C-x`       |

</div>

## Less

<div markdown="1" class="table">

| Description                            | Key Binding   |
|----------------------------------------|---------------|
| Exit pager                             | `q`           |
| Forward search \<pattern>              | `/<pattern>`  |
| Backward search \<pattern>             | `?<pattern>`  |
| Toggle search highlighting             | `ESC u ENTER` |
| Toggle status column                   | `- J ENTER`   |
| Next match                             | `n`           |
| Previous match                         | `N`           |
| Page up                                | `SPC`         |
| Page down                              | `b`           |
| Half page up                           | `d`           |
| Half page down                         | `u`           |
| One line up                            | `j`           |
| One line down                          | `k`           |
| Start of file                          | `g`           |
| End of file                            | `G`           |
| Show file info                         | `C-g`         |
| Only lines w/matching \<pattern>       | `&<pattern>`  |
| Mark position with a character \<mark> | `m<mark>`     |
| Go to \<mark>                          | `'<mark>`     |
| Open current file in `EDITOR`          | `v`**         |

</div>

\**Returns to `less` once `EDITOR` is closed.

### Options

Less can be configured via CLI options. Most options may be changed at
runtime:

<div markdown="1" class="table">

| Description                     | Key Binding             |
|---------------------------------|-------------------------|
| Save buffer to \<file>          | `-o ENTER <file> ENTER` |
| Squeeze consecutive blank lines | `-s`                    |
| Display line numbers            | `-N`                    |
| Suppress line numbers           | `-n`                    |

</div>

### Multiple Files

<div markdown="1" class="table">

| Description                | Key Binding            |
|----------------------------|------------------------|
| Open \<file1> and \<file2> | `less <file1> <file2>` |
| Open \<file3>              | `:e <file3>`           |
| Next file                  | `:n`                   |
| Previous file              | `:p`                   |

</div>

### Tail

<div markdown="1" class="table">

| Description             | Key Binding           |
|-------------------------|-----------------------|
| Open streaming file     | `less -n +F some.log` |
| Pause stream w/less nav | `C-c`                 |
| Resume stream           | `F`                   |
| Quit stream             | `C-c` then `q`        |

</div>

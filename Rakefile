# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

task :default => :validate

desc "Run Search & Deploy locally"
task :demo do
  SearchAndDeploy.demo
end

desc "Publish Search & Deploy"
task :publish do
  SearchAndDeploy.publish "./public"
end

desc "Validate Search & Deploy pages"
task :validate do
  ENV["JEKYLL_ENV"] = "testing"

  Dir.mktmpdir { |dir|
    SearchAndDeploy.publish dir
    SearchAndDeploy.validate dir
  }
end

require "html-proofer"

class SearchAndDeploy
  VALIDATION_POLICY = {
    # check_sri: true, # commented out due to google fonts
    enforce_https: false, # many references still use http only
    check_img_http: true,
    check_html: true,
    allow_hash_href: true,
    check_favicon: true,
    disable_external: true
  }

  def self.publish dir, env=ENV
    system(
      {"JEKYLL_ENV"=>env.delete("JEKYLL_ENV")},
      *%W[
        jekyll build
        -d #{dir}
        --config _config.yml,_config_production.yml
       ]
    )
  end

  def self.validate dir, policy=VALIDATION_POLICY
    HTMLProofer.check_directory(dir, policy).run
  end

  def self.demo
    system({"JEKYLL_ENV"=>"development"}, *%W[jekyll serve])
  end
end
